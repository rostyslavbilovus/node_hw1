const express = require('express');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser')

const app = express();

const PORT = 8080;

const fileExtension = ['log', 'txt', 'json', 'yaml', 'xml', 'js']

app.use(bodyParser.json())
app.use('api/files', function (req, res, next) {
    next();
});

app.get('/', (req, res) => res.send('hello world'));

app.post('/api/files', (req, res) => {

    if (!fs.existsSync(path.join(__dirname, 'files'))) {

        fs.mkdir(path.join(__dirname, 'files'), (err) => {
            if (err) {
                return console.log(err);
            };
            console.log('Directory created successfully!');

            createFile(req.body.filename, req.body.content, res);
  
            
            
        });
    } else {
        console.log('Directory already created!');

        createFile(req.body.filename, req.body.content, res);

    }
});

app.get('/api/files', (req, res) => {
         
    if (fs.existsSync(path.join(__dirname, 'files'))) {

         fs.readdir(path.join(__dirname, 'files'), (err, files) => {
         console.log(files);

           return res
            .status(200)
            .json({
                message: "Success",
                files: files
             });
        });  
    } else {
        return res
            .status(400)
            .json({ message: "Client error" });
    } 
});

app.get('/api/files/:name', (req, res) => {
    let filename = req.params.name;
    const filePath = path.join(__dirname, 'files', filename);


    if (fs.existsSync( filePath )) {
        
        fs.readFile(filePath, 'utf8', function (err, data) {
            return res
                .status(200)
                .json({
                    "message": "Success",
                    "filename": filename,
                    "content": data,
                    "extension": `.${filename.split('.').pop()}`,
                    "uploadedDate": "2022-04-14T17:32:28Z"
                });
        });
    } else {
        return res
            .status(400)
            .json({
                message: `No file with '${filename}' filename found`
            })
    }
});

app.listen(PORT, (error) => {
    error ? console.log(error) : console.log(`Server started on port ${PORT}`);
}); 

function createFile(filename, content, res) {
    const isExtensionExist = fileExtension.some(item => item.toLocaleLowerCase() === filename.split('.').pop().toLocaleLowerCase());
    const filePath = path.join(__dirname, 'files', filename);

    if (!fs.existsSync(filePath) && isExtensionExist) {
        

        fs.writeFile(filePath, content, (err) => {
            if (err)
                console.log(err);
            else {
                console.log("File written successfully");
            }
        });                
            return res
                .status(200)
                .json({ message: "File created successfully" });
            
    } else {
            return res
                .status(400)
                .json({ message: "Please specify 'content' parameter" });
            }
};
